//
//  main.swift
//  GeneticAlgorithms
//
//  Created by Sergiu Bulzan on 23/04/2017.
//  Copyright © 2017 Sergiu Bulzan. All rights reserved.
//


import Foundation


/*
 Se da un set de cuvinte W={w1,...,w2n} astfel incat fiecare cuvant wi este o secventa de caractere (un string).
 Sa se construiasca un puzzle de cuvinte incucisate folosindu-se toate cele 2n cuvinte. 
 
 Ex : Pt n=3 si W={AGE,AGO,BEG,CAB,CAD,DOG} puzzle-ul ar fi:
 
 CAB
 AGE
 DOG
 
 */


//MARK:  subscript String
extension String {
    
    var length: Int {
        return self.characters.count
    }
    
    subscript (i: Int) -> String {
        return self[Range(i ..< i + 1)]
    }
    
    func substring(from: Int) -> String {
        return self[Range(min(from, length) ..< length)]
    }
    
    func substring(to: Int) -> String {
        return self[Range(0 ..< max(0, to))]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return self[Range(start ..< end)]
    }
    
}




//MARK: add shuffle

extension MutableCollection where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffle() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in startIndex ..< endIndex - 1 {
            let j = Int(arc4random_uniform(UInt32(endIndex - i))) + i
            if i != j {
                swap(&self[i], &self[j])
            }
        }
    }
}

extension Collection {
    /// Return a copy of `self` with its elements shuffled
    func shuffled() -> [Iterator.Element] {
        var list = Array(self)
        list.shuffle()
        return list
    }
}

//MARK: Problem:

let N : Int = 3
let WORDS : [String] = ["AGE", "AGO", "BEG", "CAB", "CAD", "DOG"]

let MUT_PROB = 1
let CROSS_PROB = 5
let NO_INDIVIZI = 100
let NO_GENERATII = 30

struct Problem{
    var size : Int
    var n : Int
    var words : [String]
    
    init() {
        self.n = 3 //cuvinte
        self.size = 2 * N //numarul de cuvinte
        self.words = WORDS //cuvintele efective
    }
}

class Individual{
    var size : Int
    var chromosome : [Int]
    var fitness : Int
    
    init(_ pr: Problem) {
        self.size = pr.size
        self.chromosome = Array(0..<pr.size).shuffled()
        self.fitness = 0
    }
    
    func fitness(pr: Problem){
        var list2 = [String]()
        for i in self.chromosome.count/2..<self.chromosome.count{
            list2.append(pr.words[chromosome[i]])
        }
        
        var cuv = ""
        for k in 0..<self.size/2{
            for j in 0..<self.size/2{
                cuv.append(pr.words[self.chromosome[j]][k])
            }
            if !list2.contains(cuv){
                self.fitness += 1
            }
            cuv = ""
        }
    }
    
    func getWords(ind: [Int], pr:Problem) -> [String]{
        var lista : [String] = []
        for i in ind{
            lista.append(pr.words[i])
        }
        
        return lista
    }
    
    func mutate(prob: Int){
        var k = Int(arc4random_uniform(10))
        if k == prob{
            k = Int(arc4random_uniform(UInt32(self.size - 1))) + 1
            let a = self.chromosome[k]
            self.chromosome[k] = self.chromosome[k-1]
            self.chromosome[k-1] = a
        }
    }
    
    func crossover(i2: Individual,pr : Problem, crossProbability: Int = 5) -> Individual{
        let child = Individual(pr)
        let subs = self.size / 2 //size of subset
        
        let p :Int = Int(arc4random_uniform(10))
        if p <= crossProbability{
            var subset = [Int]()
            var k = Int(arc4random_uniform(UInt32(self.size - 1)))
            for _ in 0..<subs{
                
                k = k < self.size - 1 ? k+1 : 0
                
                child.chromosome[k] = self.chromosome[k]
                subset.append(self.chromosome[k])
            }
            var j: Int
            if k < self.size - 1{
                j = k + 1
            }else{
                j = 0
            }
            
            var ok = 0
            while ok < subs{
                if k < self.size - 1{
                    k += 1
                }else{
                    k = 0
                }
                
                if !subset.contains(i2.chromosome[k]) {
                    ok += 1
                    child.chromosome[j] = i2.chromosome[k]
                    j  = j < self.size - 1 ? j + 1 : 0
                    
                    
                }
            }
            return child
        }
        
        return self
    }
}


class Population{
    var populationSize : Int //numar de indivizi?
    var pr: Problem
    var individuals: [Individual] = []
    
    init(popSize: Int, pr:Problem){
        self.populationSize = popSize
        self.pr = pr
        for _ in 0..<self.populationSize{
            self.individuals.append(Individual(pr))
        }
    }
    
    func eval(pr : Problem){
        for ind in self.individuals{
            ind.fitness(pr: pr)
        }
    }
    
    func reunion(toAdd : Population){
        self.individuals.append(contentsOf: toAdd.individuals)
        self.populationSize = self.populationSize + toAdd.populationSize
    }
    
    func selection(n : Int){
        self.individuals.sort(by: {$0.fitness < $1.fitness}) //sort population by fitness
        self.individuals = Array(self.individuals[0..<n]) //first n elements of the array
        self.populationSize = n
    }
    
    func best(n : Int) -> [Individual]{
        let aux = self.individuals.sorted(by: {$0.fitness < $1.fitness})
        return Array(aux[0..<n])
    }
}

class Algorithm{
    var noI : Int
    var noG : Int
    let crossProb : Int
    let mutProb : Int
    var pr : Problem
    var p : Population
    
    init(){
        self.mutProb = MUT_PROB
        self.crossProb = CROSS_PROB
        self.noI = NO_INDIVIZI
        self.noG = NO_GENERATII
        
        self.pr = Problem()
        self.p = Population(popSize: self.noI, pr: pr)
        self.p.eval(pr: pr)
    }
    
    func iteration(){
        self.p.individuals.shuffle()
        let no = self.noI / 2
        let offSpring = Population(popSize: 0, pr: self.pr)
        
        for k in 0..<no{
            offSpring.individuals.append(self.p.individuals[2*k].crossover(i2: self.p.individuals[2*k + 1], pr: self.pr, crossProbability: self.crossProb ))
            offSpring.individuals[k].mutate(prob: self.mutProb)
        }
        
        offSpring.populationSize = no
        offSpring.eval(pr: pr)
        self.p.reunion(toAdd: offSpring)
        
        self.p.selection(n: self.noI)
    }
    
    func run() -> [Individual]{
        for _ in 0..<noG{
            self.iteration()
        }
        return self.p.best(n: 10)
    }
    
    func average(best: [Individual]) -> Double{
        var s = 0
        for x in best{
            print(x.chromosome)
            print(x.fitness)
            s = s + x.fitness
        }
        
        return Double(s)/Double(best.count)
    }
    
    func stand_deviation(best : [Individual], av : Double) -> Double{
        var suma = 0.0
        for x in best{
            suma += (Double(x.fitness) - av) * (Double(x.fitness) - av)
        }
        
        return sqrt(Double(suma)/Double(best.count))
    }
}

func main(){
    let alg = Algorithm()
    let result = alg.run()
    let pr = Problem()
    let ind = Individual(pr)
    
    for j in 0..<ind.getWords(ind: result[0].chromosome, pr: pr).count/2{
        print(ind.getWords(ind: result[0].chromosome, pr: pr)[j])
    }
}

main()
